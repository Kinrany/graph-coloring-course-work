#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

// matrix description
const int COLOR_NUMBER = 8;
char* COLOR_NAMES[COLOR_NUMBER]{"Blue", "Red", "Green", "Yellow", "White", "Magenta", "Darkgreen", "Gray"};

int node_number;
vector<vector<int> > edge_matrix;
vector<int> node_colors;

// matrix IO functions
void read_matrix(fstream& file) {
	file >> node_number;

	edge_matrix = vector<vector<int> >(node_number);
	for (int i = 0; i < node_number; ++i) {
		edge_matrix[i] = vector<int>(node_number);
		for (int j = 0; j < node_number; ++j) {
			file >> edge_matrix[i][j];
		}
	}

	node_colors = vector<int>(node_number, 0);
}
void write_matrix(fstream& file) {
	file << node_number << endl;

	for (int i = 0; i < node_number; ++i) {
		for (int j = 0; j < node_number; ++j) {
			file << edge_matrix[i][j] << " ";
		}
		file << endl;
	}
}
void write_colors(fstream& file) {
	file << "Colors_Nodes:" << endl;

	for (int i = 0; i < node_number; ++i) {
		int color = node_colors[i];
		if (color >= COLOR_NUMBER) color = COLOR_NUMBER - 1;
		file << i << " " << COLOR_NAMES[color] << endl;
	}
}
void print_matrix() {
	cout << node_number << endl;

	for (int i = 0; i < node_number; ++i) {
		for (int j = 0; j < node_number; ++j) {
			cout << edge_matrix[i][j] << " ";
		}
		cout << endl;
	}
}

// matrix methods
vector<pair<int, int> > swap_order;
void swap_nodes(int a, int b, int log = true) {
	int tmp;

	// horizontal swap
	for (int i = 0; i < node_number; ++i) {
		tmp = edge_matrix[a][i];
		edge_matrix[a][i] = edge_matrix[b][i];
		edge_matrix[b][i] = tmp;
	}

	// vertical swap
	for (int i = 0; i < node_number; ++i) {
		tmp = edge_matrix[i][a];
		edge_matrix[i][a] = edge_matrix[i][b];
		edge_matrix[i][b] = tmp;
	}

	// swap colors
	tmp = node_colors[a];
	node_colors[a] = node_colors[b];
	node_colors[b] = tmp;

	if (log) {
		swap_order.push_back(make_pair(a, b));
	}
}
void revert_swaps() {
	while (swap_order.size() > 0) {
		pair<int, int> ab = swap_order.back();
		swap_order.pop_back();

		swap_nodes(ab.first, ab.second, false);
	}
}
void sort_by_degree() {
	// calculate degree of every node
	vector<int> degrees(node_number, 0);
	for (int i = 0; i < node_number; ++i) {
		for (int j = 0; j < node_number; ++j) {
			if (edge_matrix[i][j]) degrees[i]++;
		}
	}

	// sort nodes by degree ascending
	// stupid sort because coloring is more costly anyway
	for (int i = 0; i < node_number; ++i) {
		for (int j = i + 1; j < node_number; ++j) {
			if (degrees[i] > degrees[j]) {
				int tmp = degrees[i];
				degrees[i] = degrees[j];
				degrees[j] = tmp;

				swap_nodes(i, j);
			}
		}
	}
}

// coloring algorithms
void greedy_coloring() {
	node_colors = vector<int>(node_number, -1);
	int colored_count = 0;
	for (int color = 0; color < node_number && colored_count < node_number; ++color) {
		for (int node = 0; node < node_number; ++node) {
			if (node_colors[node] != -1) continue;

			bool valid = true;
			for (int neighbor = 0; neighbor < node_number && valid; ++neighbor) {
				if (edge_matrix[node][neighbor] && node_colors[neighbor] == color) {
					valid = false;
				}
			}

			if (valid) {
				node_colors[node] = color;
				colored_count++;
			}
		}
	}
}
void color_matrix() {
	sort_by_degree();
	greedy_coloring();
	revert_swaps();
}


int main(int argc, char *argv[]) {
	if (argc < 2) {
		cout << "No file path" << endl;
		return 0;
	}
	char* filepath = argv[1];

	// read matrix from filepath
	fstream file;
	file.open(filepath);
	read_matrix(file);
	file.close();

	color_matrix();

	// write matrix to filepath
	file.open(filepath);
	file.clear();
	write_matrix(file);
	write_colors(file);
	file.close();

	return 0;
}




